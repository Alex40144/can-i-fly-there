import browser from "webextension-polyfill";
import { getStorage } from "~storage";

function onImportSubmit(event: any) {
    event.preventDefault();

    const fileField = document.querySelector<HTMLInputElement>("#import-file");

    const file = fileField!.files![0];
    if (!file) {
        return;
    }

    const reader = new FileReader();
    reader.onload = function(event) {
        const contents = event.target!.result as string;

        const icaoEntries = JSON.parse(contents);
        browser.storage.local.set({ icaoEntries: icaoEntries });

        alert("Imported " + Object.keys(icaoEntries).length + " ICAOs");
    };
    reader.readAsText(file);
}

async function onExport(event: any) {
    const storage = await getStorage();

    const json = JSON.stringify(storage.icaoEntries);
    const blob = new Blob([json], {type: "application/json"});

    browser.downloads.download({
        url: URL.createObjectURL(blob),
        filename: "caniflythere.json"
    });
}

function onClear() {
    const sureInput = document.querySelector("#clear-checkbox") as HTMLInputElement;

    if (sureInput.checked) {
        browser.storage.local.set({ icaoEntries: {} });
        alert("Cleared all ICAOs!");
    }
}

function onLoaded() {
    document.querySelector("#import-form")!.addEventListener("submit", onImportSubmit);
    document.querySelector("#export-button")!.addEventListener("click", onExport);
    document.querySelector("#clear-button")!.addEventListener("click", onClear);
}

document.addEventListener("DOMContentLoaded", onLoaded);
