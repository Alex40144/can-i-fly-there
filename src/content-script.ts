import { IcaoEntry, getStorage, StorageData } from "~/storage";

function getStatus(icaoEntries: Record<string, IcaoEntry>, airport: string) {
    const entry = icaoEntries[airport];

    if (entry) {
        return entry.status;
    } else {
        return "unknown";
    }
}

function getSimIcao(icaoEntries: Record<string, IcaoEntry>, airport: string) {
    const entry = icaoEntries[airport];
    return entry.simIcao;
}

function createTagForAirport(icaoEntries: Record<string, IcaoEntry>, airport: string) {
    const tag = document.createElement("a");

    tag.style.fontSize = "1.0rem";
    tag.style.padding = "2px 4px";
    tag.style.borderRadius = "4px";
    tag.style.color = "white";

    const status = getStatus(icaoEntries, airport);

    if (status == "exists") {
        tag.innerHTML = "✓";
        tag.title = "You can fly to " + airport + "!";

        tag.style.background = "green";
    } else if (status == "missing") {
        tag.innerHTML = "✗";
        tag.title = "You can't fly to " + airport + "!";

        tag.style.background = "red";
    } else if (status == "renamed") {
        tag.innerHTML = "✓";
        tag.title = "You can fly to " + airport + ", as " + getSimIcao(icaoEntries, airport) + "!";

        tag.style.background = "orange";
    } else {
        tag.innerHTML = "?";
        tag.title = airport + " isn't listed in your database.";

        tag.style.background = "grey";
    }

    return tag;
}

function injectIcaoTags(storage: StorageData) {
    // Extract the airport name from airport pages
    let currentAirportIcao = "XXXX";

    const airportInfo = document.querySelector(".airportInfo");
    if (airportInfo != null) {
        const heading = airportInfo.querySelector("h1");
        if (heading != null) {
            currentAirportIcao = heading.innerText;

            const tag = createTagForAirport(storage.icaoEntries, currentAirportIcao);
            heading.appendChild(tag);
        }
    }

    // Find all links to airports
    const links = document.getElementsByTagName("a");
    for (let i = 0; i < links.length; i++) {
        const link: HTMLAnchorElement = links[i];

        if (link.href.includes("airport.jsp") &&
            link.href.includes("icao=") &&
            !link.href.includes(currentAirportIcao)
        ) {
            const icao = link.href.split("icao=")[1].split("?")[0];

            const tag = createTagForAirport(storage.icaoEntries, icao);
            link.parentNode!.appendChild(tag);
        }
    }
}

function injectAircrafts(storage: StorageData) {
    // Replace everything in the aircraft table
    var aircraftTable = document.querySelector(".aircraftTable");
    var aircraftConfigs = false;

    if (aircraftTable == null) {
        var aircraftTable = document.querySelector(".configTable");
        var aircraftConfigs = true;
        if (aircraftTable == null) {
            console.log("uh oh");
        }
    }

    const rows = aircraftTable!.querySelectorAll("tr");
    console.log(rows)
    for (let row = 0; row < rows.length; row++) {
        const columns = rows[row].querySelectorAll("td");

        if (columns.length == 0) {
            continue;
        }

        var airplaneColumn = columns[1];
        if (aircraftConfigs) {
            var airplaneColumn = columns[0];
        }

        const mapping = Object.keys(storage.availableAircrafts)
            .find(v => airplaneColumn.innerText?.includes(v));

        if (mapping != undefined) {
            const text = airplaneColumn.innerText;

            const newColumn = document.createElement("td");

            const columnLink = document.createElement("a");
            columnLink.innerText = text;
            columnLink.title = storage.availableAircrafts[mapping];
            columnLink.style.color = "green";

            newColumn.appendChild(columnLink);

            airplaneColumn.replaceWith(newColumn);
        } else {
            airplaneColumn.style.textDecorationLine = "line-through";
            airplaneColumn.style.color = "grey";
        }
    }

    // Replace everything in the airport search's model table
    const selects = document.querySelectorAll<HTMLSelectElement>(".formselect");
    
    for (let select = 0; select < selects.length; select++) {
        if (selects[select].name != "model") {
            continue;
        }

        const items = selects[select].querySelectorAll("option");
        for (let item = 0; item < items.length; item++) {
            const mapping = Object.keys(storage.availableAircrafts)
                .find(v => items[item].innerText?.includes(v));

            if (mapping == undefined) {
                continue;
            }

            items[item].innerText = items[item].innerText + " ✓";
        }
    }
}

function injectCopyCoodatinesButton() {
    const airportInfo = document.querySelector(".airportInfo");

    if (airportInfo == null) {
        return;
    }

    let regex = /^\s*Lat: [0-9\.]*[NS], Long: [0-9\.]*[WE]\s*$/;

    let divs = airportInfo.querySelectorAll("div");

    for (let i = 0; i < divs!.length; i++) {
        const div = divs[i];

        // Check if this is a text only node
        let isText = true;
        for (let x = 0; x < div.children.length; x++) {
            if (div.children[x].nodeType != Node.TEXT_NODE) {
                isText = false;
                break;
            }
        }

        if (!isText) {
            continue;
        }

        // Make sure we've got the right node
        if (!regex.test(div.innerText)) {
            continue;
        }

        // We do have the right node, extract the coordinates
        const parts = div.innerText.split(", ");
        const lat = parts[0].substring(5);
        const long = parts[1].substring(6);

        const button = document.createElement("button");
        button.innerText = "Copy";

        button.onclick = () => {
            navigator.clipboard.writeText(lat + " " + long);
        };

        div.appendChild(button);
    }
}

function onEntriesLoaded(storage: StorageData) {
    if (storage.enableIcaos) {
        injectIcaoTags(storage);
    }

    if (storage.enableAircrafts) {
        injectAircrafts(storage);
    }

    if (storage.enableCopyCoordinates) {
        injectCopyCoodatinesButton();
    }
}

// First retrieve all entries
getStorage().then(onEntriesLoaded);
