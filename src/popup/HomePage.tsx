import browser from "webextension-polyfill"
import React, { Component, FormEvent, ChangeEvent, MouseEvent } from "react"

import Heading from "~components/heading"
import { getStorage } from "~storage"

type HomePageProps = {
    onNavigate: (route: string, props: any) => void;
}

type HomePageState = {
    icaoCount: number;
    findIcao: string,
}

class HomePage extends Component<HomePageProps, HomePageState> {
    constructor(props: HomePageProps) {
        super(props)
        
        this.state = {
            icaoCount: 0,
            findIcao: "",
        }
    }

    componentDidMount = async () => {
        const storage = await getStorage();
        this.setState({ icaoCount: Object.keys(storage.icaoEntries).length });
    }

    onIacoInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        const iaco = event.target.value.toUpperCase();

        if (iaco.match(/^[A-Z0-9]*$/) ) {
            this.setState({ findIcao: iaco });
        } else {
            event.preventDefault();
        }
    }

    onFindSubmit = (event: FormEvent) => {
        event.preventDefault();

        if (this.state.findIcao != "") {
            this.props.onNavigate("icao", this.state.findIcao);
        }
    }

    onAircraftsClick = (event: MouseEvent) => {
        event.preventDefault();

        browser.tabs.create({
            url: "../page-aircrafts/page.html"
        });
    }

    onFeaturesClick = (event: MouseEvent) => {
        event.preventDefault();

        browser.tabs.create({
            url: "../page-features/page.html"
        });
    }

    onImportExportClick = (event: MouseEvent) => {
        event.preventDefault();

        browser.tabs.create({
            url: "../page-import/page.html"
        });
    }

    render() {
        return (
            <div>
                <Heading>Find ICAO</Heading>
                <form className="margin-bottom-2 flex" onSubmit={this.onFindSubmit}>
                    <input
                        type="text flex-grow"
                        onChange={this.onIacoInputChange}
                        value={this.state.findIcao}
                    />
                    <button type="submit">GO</button>
                </form>

                <Heading>Options</Heading>
                <div className="margin-bottom-2">
                    <div className="margin-bottom-1">
                        <a href="#" onClick={this.onAircraftsClick}>Aircrafts</a>
                    </div>
                    <div className="margin-bottom-1">
                        <a href="#" onClick={this.onFeaturesClick}>Features</a>
                    </div>
                    <div>
                        <a href="#" onClick={this.onImportExportClick}>Import/Export ICAOs</a>
                    </div>
                </div>

                <Heading>Statistics</Heading>
                <ul>
                    <li>ICAOs: {this.state.icaoCount}</li>
                </ul>
            </div>
        )
    }
}

export default HomePage;