import React, { Component } from "react"

import HomePage from "~popup/HomePage";
import IcaoPage from "~popup/IcaoPage";

type PopupState = {
    route: string,
    props: string,
}

class Popup extends Component<{}, PopupState> {
    constructor(props: {}) {
        super(props)
        
        this.state = {
            route: "home",
            props: "",
        }
    }

    onNavigate = (route: string, props: any) => {
        this.setState({
            route: route,
            props: props,
        });
    }

    render() {
        let route = <div>No Route</div>;

        switch (this.state.route) {
            case "home":
                route = <HomePage onNavigate={this.onNavigate} />
                break;
            case "icao":
                route = <IcaoPage onNavigate={this.onNavigate} props={this.state.props} />
                break;
        }

        return (
            <div style={{width: "250px"}}>
                {route}
            </div>
        )
    }
}

export default Popup;