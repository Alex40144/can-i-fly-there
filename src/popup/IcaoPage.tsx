import React, { Component, FunctionComponent, ChangeEvent, FormEvent } from "react"
import browser from "webextension-polyfill"
import Heading from "~components/heading"
import { getStorage, IcaoEntry } from "~storage"

const IcaoTag: FunctionComponent<{icao: IcaoEntry | null}> = (props) => {
    let content = "?";
    let background = "grey";
    let color = "white";

    if (props.icao) {
        switch (props.icao.status) {
            case "exists":
                content = "✓";
                background = "green";
                break;
            case "renamed":
                content = "✓";
                background = "orange";
                break;
            case "missing":
                content = "✗";
                background = "red";
                break;
        }
    }

    return (
        <div style={{
            color: color,
            background: background,
            borderRadius: 4,
            fontSize: "1rem",
            padding: "2px 4px",
            display: "inline"
        }}>{content}</div>
    )
}

async function onIcaoRemoveClicked(icao: string) {
    let icaoEntries = (await getStorage()).icaoEntries;

    delete icaoEntries[icao];

    browser.storage.local.set({ icaoEntries: icaoEntries });
}

const IcaoExists: FunctionComponent<{icao: string, onRefresh: () => void}> = (props) => {
    return (
        <div>
            <div className="margin-bottom-1">
                This ICAO exists in your simulator.
            </div>
            <button onClick={async () => {
                await onIcaoRemoveClicked(props.icao);
                props.onRefresh();
            }}>Remove</button>
        </div>
    );
}

const IcaoRenamed: FunctionComponent<{icao: string, simIcao: string, onRefresh: () => void}> = (props) => {
    return (
        <div>
            <div className="margin-bottom-1">
                This ICAO exists, but is named "{props.simIcao}" in your simulator.
            </div>
            <button onClick={async () => {
                await onIcaoRemoveClicked(props.icao);
                props.onRefresh();
            }}>Remove</button>
        </div>
    );
}

const IcaoMissing: FunctionComponent<{icao: string, onRefresh: () => void}> = (props) => {
    return (
        <div>
            <div className="margin-bottom-1">
                This ICAO is missing in your simulator.
            </div>
            <button onClick={async () => {
                await onIcaoRemoveClicked(props.icao);
                props.onRefresh();
            }}>Remove</button>
        </div>
    );
}

class IcaoUnknown extends Component<{icao: string, onRefresh: () => void}, {simIcao: string}> {
    constructor(props: {icao: string, onRefresh: () => void}) {
        super(props);

        this.state = { simIcao: "" };
    }

    onIacoInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        const iaco = event.target.value.toUpperCase();

        if (iaco.match(/^[A-Z0-9]*$/) ) {
            this.setState({ simIcao: iaco });
        } else {
            event.preventDefault();
        }
    }

    onSubmit = async (event: FormEvent) => {
        event.preventDefault();

        let icaoEntries = (await getStorage()).icaoEntries;
        if (this.state.simIcao == "" || this.state.simIcao == this.props.icao) {
            icaoEntries[this.props.icao] = { status: "exists" };
        } else {
            icaoEntries[this.props.icao] = { status: "renamed", simIcao: this.state.simIcao };
        }

        browser.storage.local.set({ icaoEntries: icaoEntries });
        this.props.onRefresh();
    }

    render() {
        return (
            <div>
                <div className="margin-bottom-1">
                    This ICAO is not listed.
                </div>
                <form onSubmit={this.onSubmit}>
                    <label style={{display: "block"}}>
                        <div>Name in Simulator (optional)</div>
                        <input type="text" onChange={this.onIacoInputChange} value={this.state.simIcao} />
                    </label>
                    <button type="submit">Add</button>
                </form>
            </div>
        );
    }
}

type IcaoPageProps = {
    props: any;
    onNavigate: (route: string, props: any) => void;
}

type IcaoPageState = {
    icaoEntry: IcaoEntry | null;
}

class IcaoPage extends Component<IcaoPageProps, IcaoPageState> {
    constructor(props: IcaoPageProps) {
        super(props);

        this.state = { icaoEntry: null };
    }

    componentDidMount = async () => {
        this.onRefresh();
    }

    onRefresh = async () => {
        const storage = await getStorage();

        const icaoEntry = storage.icaoEntries[this.props.props];

        if (icaoEntry == undefined) {
            this.setState({ icaoEntry: null });
        } else {
            this.setState({ icaoEntry: icaoEntry });
        }
    }

    render() {
        let icaoContent;
        
        if (this.state.icaoEntry) {
            switch (this.state.icaoEntry.status) {
                case "exists":
                    icaoContent = <IcaoExists onRefresh={this.onRefresh} icao={this.props.props} />;
                    break;
                case "renamed":
                    icaoContent = <IcaoRenamed
                        onRefresh={this.onRefresh}
                        icao={this.props.props}
                        simIcao={this.state.icaoEntry.simIcao!}
                    />;
                    break;
                case "missing":
                    icaoContent = <IcaoMissing onRefresh={this.onRefresh} icao={this.props.props} />
                    break;
            }
        } else {
            icaoContent = <IcaoUnknown onRefresh={this.onRefresh} icao={this.props.props} />;
        }

        return (
            <div>
                <a
                    href="#"
                    onClick={(e) => this.props.onNavigate("home", {})}
                    className="margin-bottom-1 display-block"
                >
                    &lt;- Go Back
                </a>
                <Heading>
                    ICAO: {this.props.props}&nbsp;
                    <IcaoTag icao={this.state.icaoEntry} />
                </Heading>
                {icaoContent}
            </div>
        );
    }
}

export default IcaoPage;
